#include "Floor.hpp"

Floor::Floor() {
    // 生成房间地形的函数

    terrian = {
        "|--------|",
        "|........|",
        "|........|",
        "|........|",
        "|--------|"};
    
    // 设置玩家位置
    pc.set_pos({3, 4});
}

void Floor::print_to(std::ostream & os) {
    // 输出地图

    char canvas[height][width]; // “画布”，起到缓冲区的作用

    // 先绘制地形
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            canvas[i][j] = terrian[i][j];
        }
    }

    os << "\033[H\033[J";  // 将终端的光标移动到左上方再输出，效果约等于清屏

    // 将角色画在画布上
    canvas[pc.get_pos().row][pc.get_pos().col] = pc.get_denote();
    // 如果有多个角色，这里便是使用循环遍历角色列表

    // 将画布上的内容输出
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            os << canvas[i][j];
        }
        os << "\n";
    }

    // 在进行后续操作前 flush 一下，保证输出得到及时显示
    os << std::flush;
}
