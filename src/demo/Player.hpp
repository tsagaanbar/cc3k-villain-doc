#ifndef PLAYER_HPP
#define PLAYER_HPP

struct Point {
    int row;
    int col;
};

class Player {
private:
    Point pos;

public:
    Player() {}
    auto set_pos(Point pos_) {
        this->pos = pos_;
    }
    auto get_pos() { return this->pos; }
    auto get_denote() {
        return '@';
    }
    ~Player() {}
};

#endif
