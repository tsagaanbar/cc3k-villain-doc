#ifndef ANIMAL_HPP
#define ANIMAL_HPP

#include "AnimalHitter.hpp"

#include <iostream>

class Animal {
public:
    virtual void be_hit_by(AnimalHitter *) = 0;
    virtual void bite(Human *) = 0;
    virtual ~Animal() = default;
};

class Dog : public Animal {
public:
    void be_hit_by(AnimalHitter * hitter) override {
        hitter->hit(this); // match AnimalHitter::hit(Dog*)
        // or call AnimalHitter::hitDog(Animal *)
        std::cout << "Dog: Woof, woof!" << std::endl;
    }
    void bite(Human * human) override {
        std::cout << "Dog: [bite human]" << std::endl;
    }
    ~Dog() = default;
};

class Cat : public Animal {
public:
    void be_hit_by(AnimalHitter * hitter) override {
        hitter->hit(this); // match AnimalHitter::hit(Cat*)
        // or call AnimalHitter::hitCat(Animal *)
        std::cout << "Cat: Meow!" << std::endl;
    }
    void bite(Human * human) override {
        std::cout << "Cat: [bite human]" << std::endl;
    }
    ~Cat() = default;
};

#endif  // ANIMAL_HPP
