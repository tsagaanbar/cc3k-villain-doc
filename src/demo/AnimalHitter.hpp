#ifndef ANIMAL_HITTER_HPP
#define ANIMAL_HITTER_HPP

class Animal;
class Cat;
class Dog;

class AnimalHitter {
public:
    virtual void hit(Cat *) = 0; // or: void hitCat(Animal *);
    virtual void hit(Dog *) = 0; // or: void hitDog(Animal *);
    virtual void be_biten(Animal * p) = 0;
    virtual ~AnimalHitter() = default;
};

class Human : public AnimalHitter {
public:
    void hit(Cat * p) override;
    void hit(Dog * p) override;
    void be_biten(Animal * p) override;
    ~Human() = default;
};

// 由于 Cat 和 Dog 还未具体定义，属于“不完整类型”，
// 因此这里不能直接在 Human 类里 inline 方式定义函数，
// 需要在类外定义函数

#endif  // ANIMAL_HITTER_HPP
