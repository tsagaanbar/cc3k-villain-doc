#include <iostream>
#include <string>

#include "Floor.hpp"

int main() {
    // 总控制流程

    Floor floor;
    std::string input;

    while (true) {
        // 显示当前状态
        floor.print_to(std::cout);

        // 等待用户输入
        std::getline(std::cin, input);
        
        if (input == "#") break;

        // 处理用户输入
        auto newPos = floor.get_player_pos();

        if (input == "w") { newPos.row -= 1; }
        if (input == "a") { newPos.col -= 1; }
        if (input == "s") { newPos.row += 1; }
        if (input == "d") { newPos.col += 1; }

        if (floor.is_avaliable_pos(newPos)) {
            floor.move_player(newPos);
        }

        // 游戏状态更新，如房间中实体的移动
    }
}

